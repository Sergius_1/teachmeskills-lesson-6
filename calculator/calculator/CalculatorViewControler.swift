//
//  CalculatorViewControler.swift
//  calculator
//
//  Created by Siarhei Dubko on 7/30/20.
//  Copyright © 2020 Siarhei Dubko. All rights reserved.
//

import UIKit

enum Operation {
    case none, plus, minus, devide, multiple
}


class CalculatorViewController: UIViewController {
    var currentResult: Double = 0
    var isNeedToClearResultLabel: Bool = false
    var value2: Double = 0
    var operation: Operation = .none
    
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBAction func digitClicked(_ sender: UIButton) {
        if isNeedToClearResultLabel == true {
            resultLabel.text = "0"
            isNeedToClearResultLabel = false
            
        }
        
        if resultLabel.text == "0" {
            resultLabel.text = sender.currentTitle
        } else {
            resultLabel.text = resultLabel.text! + sender.currentTitle!
        }
        isNeedToClearResultLabel = true
    }
    
    @IBAction func operationClicked(_ sender: UIButton) {
        if sender.currentTitle == "+" {
            operation = .plus
        } else if sender.currentTitle == "-" {
            operation = .minus
        } else if sender.currentTitle == "*" {
            operation = .multiple
        } else if sender.currentTitle == "/" {
            operation = .devide
        }
        currentResult = Double(resultLabel.text!)!
    }
    
    @IBAction func resultClicked(_ sender: UIButton) {
        var result: Double = 0
        let value2 = Double(resultLabel.text!)!
        switch operation {
        case .plus:
            result = currentResult + value2
        case .minus:
            result = currentResult - value2
        case .devide:
            result = currentResult / value2
        case .multiple:
            result = currentResult * value2
        default:
            break
        }
        resultLabel.text = String(result)
        operation = .none
        isNeedToClearResultLabel = true
        
    }
    
}





